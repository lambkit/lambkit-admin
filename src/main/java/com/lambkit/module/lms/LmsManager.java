package com.lambkit.module.lms;

import com.lambkit.Lambkit;

public class LmsManager {

	private static final LmsManager manager = new LmsManager();
	
	public static LmsManager me() {
		return manager;
	}
	
	public boolean isLmsActived() {
		LmsConfig lmsconfig = Lambkit.config(LmsConfig.class);
		return lmsconfig.isEnable();
	}
	
}
